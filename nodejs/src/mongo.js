//Object data modelling library for mongo
const mongoose = require('mongoose');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express');

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//use the amqplib
const amqp = require('amqplib/callback_api');

//use the axios library
const axios = require('axios');

//instance of express and port to use for inbound connections.
const app = express();
const port = 3000;

//nodes array
let nodesList = [];

//leader
let sysLeader = 1;

let d = new Date();
let startTime = d.getTime();

//Get the hostname of the node
var os = require("os");
//const { name } = require('body-parser');
var myhostname = os.hostname();

console.log(myhostname);
console.log(myhostname);
console.log(myhostname);
console.log(myhostname);
console.log(myhostname);


var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
toSend = {"nodeName": myhostname, "status": "alive", "nodeID": nodeID}

//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/notFlixDB?replicaSet=rs0';



setInterval(function() {

  console.log(`Intervals are used to fire a function for the lifetime of an application.`);

}, 3000);

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var analysisSchema = new Schema({
  _id: Number,
  username: String,
  titleID: Number,
  userAction: String,
  dateTime: String,
  interactionPoint: String,
  interactionType: String
});

var analysisModel = mongoose.model('Analysis', analysisSchema, 'analysis');



app.get('/', (req, res) => {
  analysisModel.find({},'username userAction interactionPoint dateTime', (err, analysis) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(analysis))
  }) 
})

app.post('/',  (req, res) => {
  var new_notflix = new analysisModel(req.body);
  new_notflix.save(function (err) {
  if (err) res.send('Error');
    res.send(JSON.stringify(req.body))
  });
})

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})

function pubsub(){

  setInterval(function() {

    amqp.connect('amqp://test:test@cloudcomputing6130-863040-haproxy-1', function(error0, connection) {

    //if connection failed throw error
    if (error0) {
        throw error0;
    }

    //create a channel if connected and send hello world to the logs Q
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        var exchange = 'logs';
        var msg =  JSON.stringify(toSend);

        channel.assertExchange(exchange, 'fanout', {
                durable: false
        });
        
        channel.publish(exchange, '', Buffer.from(msg));
        console.log(" [x] Sent %s", msg);
     });

           
     //in 1/2 a second force close the connection
     setTimeout(function() {
         connection.close();
     }, 500);
     });

}, 3000);


amqp.connect('amqp://test:test@cloudcomputing6130-863040-haproxy-1', function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                                channel.bindQueue(q.queue, exchange, '');

                                channel.consume(q.queue, function(msg) {
                                            if(msg.content) {
                                                            let currTime = new Date();
                                                            let c = -(startTime - currTime.getTime());
                                                            c /= 1000;
                                                            //console.log(c);
                                                            //console.log(myhostname + " -> " + msg.content)
                                                            let nodeName = JSON.parse(msg.content.toString("utf-8")).nodeName;
                                                            let newNodeID = JSON.parse(msg.content.toString("utf-8")).nodeID
                                                            nodesList.some(nodes => nodes.nodeName === nodeName) ? (nodesList.find(e => e.nodeName === nodeName)).timeAlive = c : nodesList.push({"nodeID":newNodeID,"nodeName":nodeName, "timeAlive":c});                                                            
                                                            console.log(nodesList);
                                                          }
                                          }, {
                                                      noAck: true
                                                    });
                              });
            });
});


//check if leader
setInterval(function() {
  let newTime = new Date().getTime();
  let a = -(startTime - newTime);
  let timeDiff = a / 1000;
  let leader = 1;

  nodesList.forEach(function(e){
    let timeSince = timeDiff - e.timeAlive;
    console.log("Time since: " + timeSince);
    //let timeSince = a - e.timeAlive;
    console.log("node timeSeen: " + e.timeAlive);
    //console.log("Node Time Seen: " + e.timeAlive)
    if(e.nodeName === myhostname || timeSince > 10) {
    }
    else {
      if (e.nodeID > nodeID) {
        console.log("For each node ID" + e.nodeID);
        console.log("NodeID already existing" + nodeID);
        leader = 0;
      }
    }

  });
  sysLeader = (leader === 1) ? 1 : 0;

}, 15000);

setInterval(function() {
  

  if (sysLeader === 1) {
    console.log("I am the leader!");

    nodesList.forEach(function(e){
      let newTime = new Date().getTime();
      let a = -(startTime - newTime);
      let timeDiff = a / 1000;
      let timeSince = timeDiff - e.timeAlive;
      if(timeSince > 10) {
        createContainer(e.nodeName);
      }
    });
  }

}, 5000);
}

setTimeout(pubsub,20000);

async function createContainer(containerName){
  try{
      await axios.post(`http://host.docker.internal:2375/containers/${containerName}/start`);
  }
  catch(error)
  {
      console.log(error);
  }
}



